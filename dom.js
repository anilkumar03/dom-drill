/* Select the section with an id of container without using querySelector */

var sampleContainer = document.getElementById('container');


/*Select the section with an id of container using querySelector*/ 

var container = document.querySelector('#container')



/*Select all of the list items with a class of "second"*/

var secondElments = document.querySelectorAll('.second')
console.log(secondElments)

/*Select a list item with a class of third, but only the list item inside of the ol tag.*/

var orderedLists = document.querySelector('ol');
console.log(orderedLists.children[2].textContent)


/*Give the section with an id of container the text "Hello!"*/

var sectionPart = document.getElementById('container')
sectionPart.innerHTML += 'Hello'


/*Add the class main to the div with a class of footer.*/

var footerPart = document.querySelector('.footer')
footerPart.className = 'main'

// /*Remove the class main on the div with a class of footer.*/

footerPart.classList.remove('main')


/*Create a new li element. */

var newList = document.createElement('li')


/*Give the li the text "four". */

newList.textContent = 'four'


/*Append the li to the ul element. */

var unorderedLists = document.querySelector('ul')
unorderedLists.appendChild(newList)


/*Loop over all of the lis inside the ol tag and give them a background color of "green". */

var orderedLists = document.querySelector('ol')
for(i=0; i<orderedLists.children.length; i++){
    orderedLists.children[i].style.backgroundColor = 'green'
    orderedLists.children[i].style.margin = '10px'
}


/*Remove the div with a class of footer. */

footerPart.textContent = 'Anil Kumar'        /*for check */
footerPart.remove()